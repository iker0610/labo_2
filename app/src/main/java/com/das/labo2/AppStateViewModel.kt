package com.das.labo2

import android.content.Context
import android.content.res.Configuration
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import com.das.labo2.utils.getActivity
import java.util.*


enum class AppLanguage(val language: String, val code: String) {
    EN("English", "en"),
    ES("Español", "es"),
}

enum class AppLifecycleState {
    OnCreate,
    OnStart,
    OnResume,
    OnRestart,
    OnPause,
    OnStop,
    OnDestroy
}

class AppStateViewModel : ViewModel() {
    var currentLang by mutableStateOf(this.getDefaultLocale())
        private set

    val transitionedStates = mutableStateListOf<AppLifecycleState>()
    private val transitionedStatesCounter = mutableStateMapOf<AppLifecycleState, Int>()

    val numberRotations: Int
        get() = transitionedStatesCounter[AppLifecycleState.OnStop] ?: 0

    fun addState(state: AppLifecycleState) {
        transitionedStates.add(state)
        transitionedStatesCounter[state] = transitionedStatesCounter[state]?.plus(1) ?: 1
    }

    fun changeLang(lang: AppLanguage, context: Context, recreate: Boolean = true) {
        if (lang != currentLang || currentLang.code != Locale.getDefault().language) {
            currentLang = lang

            context.resources.apply {
                val locale = Locale(lang.code)
                val config = Configuration(configuration)

                context.createConfigurationContext(configuration)
                Locale.setDefault(locale)
                config.setLocale(locale)
                context.resources.updateConfiguration(config, displayMetrics)
            }
            if (recreate) context.getActivity()?.recreate()
        }
    }

    private fun getDefaultLocale(): AppLanguage {
        return try {
            AppLanguage.valueOf(Locale.getDefault().language.uppercase())
        } catch (e: IllegalArgumentException) {
            AppLanguage.ES
        }
    }
}

//----------------------------------------------------------------

