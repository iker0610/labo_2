package com.das.labo2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.ui.platform.LocalContext
import com.das.labo2.ui.Labo2App
import com.das.labo2.ui.theme.Labo_2Theme
import com.das.labo2.utils.rememberWindowSizeClass

class MainActivity : ComponentActivity() {

    // Guardamos el estado de la app
    private val appViewModel by viewModels<AppStateViewModel>()

    // Definimos el método on create
    override fun onCreate(savedInstanceState: Bundle?) {
        // Llamamos al super
        super.onCreate(savedInstanceState)

        // Actualizamos nuestro estado
        appViewModel.addState(AppLifecycleState.OnCreate)

        // Definimos la Interfaz
        setContent {
            val windowSizeClass = rememberWindowSizeClass()
            appViewModel.changeLang(appViewModel.currentLang, LocalContext.current, false)

            Labo_2Theme {
                Labo2App(appViewModel, windowSizeClass)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        appViewModel.addState(AppLifecycleState.OnStart)
    }

    override fun onResume() {
        super.onResume()
        appViewModel.addState(AppLifecycleState.OnResume)
    }

    override fun onRestart() {
        super.onRestart()
        appViewModel.addState(AppLifecycleState.OnRestart)
    }

    override fun onPause() {
        super.onPause()
        appViewModel.addState(AppLifecycleState.OnPause)
    }

    override fun onStop() {
        super.onStop()
        appViewModel.addState(AppLifecycleState.OnStop)
    }

    override fun onDestroy() {
        super.onDestroy()
        appViewModel.addState(AppLifecycleState.OnDestroy)
    }
}
