package com.das.labo2.ui

import android.content.Context
import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Home
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.das.labo2.AppLanguage
import com.das.labo2.AppStateViewModel
import com.das.labo2.R
import com.das.labo2.utils.WindowSize


@Composable
fun Labo2App(appViewModel: AppStateViewModel, windowSize: WindowSize) {
    val isCompact = windowSize == WindowSize.Compact


    Scaffold(
        topBar = { AppBar() }
    ) {
        MainContent(appViewModel, isCompact)
    }
}

//--------------------------------------------------------------

@Composable
private fun AppBar() {
    TopAppBar(
        navigationIcon = {
            Icon(
                imageVector = Icons.Rounded.Home,
                contentDescription = null,
                modifier = Modifier.padding(horizontal = 12.dp),
            )
        },
        title = {
            Text(text = stringResource(R.string.app_title))
        },
        backgroundColor = MaterialTheme.colors.primarySurface
    )
}

@Composable
private fun MainContent(appViewModel: AppStateViewModel, isCompact: Boolean) {
    if (isCompact) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            LoggerTextBoxSection(appViewModel, Modifier.fillMaxHeight(0.65f))
            LanguageSelectionSection(
                onClick = appViewModel::changeLang,
                currentLang = appViewModel.currentLang
            )
        }
    } else {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceAround,
        ) {
            LoggerTextBoxSection(appViewModel, Modifier.fillMaxWidth(0.65f))
            LanguageSelectionSection(
                onClick = appViewModel::changeLang,
                currentLang = appViewModel.currentLang
            )
        }
    }
}

//-------------------------------------------------------------------------------

@Composable
private fun LoggerTextBoxSection(appViewModel: AppStateViewModel, modifier: Modifier = Modifier) {
    Column(
        modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        LoggerTextBox(
            title = stringResource(R.string.num_rotations_title),
            text = appViewModel.numberRotations.toString(),
            modifier = Modifier
                .fillMaxWidth(0.7f)
                .fillMaxHeight(0.25f)
                .wrapContentHeight())
        LoggerTextBox(
            title = stringResource(R.string.transitioned_states_title),
            text = appViewModel.transitionedStates.joinToString(),
            modifier = Modifier
                .fillMaxWidth(0.7f)
                .fillMaxHeight(0.7f)
                .wrapContentHeight()
        )
    }
}

@Composable
private fun LoggerTextBox(modifier: Modifier = Modifier, title: String, text: String) {
    Card(
        elevation = 8.dp,
        modifier = modifier
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(text = title, textAlign = TextAlign.Center, style = MaterialTheme.typography.h6)
            Text(
                text = text,
                textAlign = TextAlign.Center,
                modifier = Modifier.verticalScroll(
                    rememberScrollState())
            )
        }
    }
}

@Composable
private fun LanguageSelectionSection(
    modifier: Modifier = Modifier,
    onClick: (AppLanguage, Context) -> Unit,
    currentLang: AppLanguage,
) {
    Column(
        modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        enumValues<AppLanguage>().forEach { lang ->
            LanguageButton(
                modifier = Modifier.fillMaxWidth(0.7f),
                lang = lang,
                onClick = onClick,
                enabled = lang != currentLang
            )
        }
    }
}

@Composable
private fun LanguageButton(
    modifier: Modifier = Modifier,
    lang: AppLanguage,
    onClick: (AppLanguage, Context) -> Unit,
    enabled: Boolean = true,
) {
    val context = LocalContext.current

    Button(
        modifier = modifier,
        onClick = { onClick(lang, context) },
        enabled = enabled
    ) {
        Text(text = lang.language)
    }
}